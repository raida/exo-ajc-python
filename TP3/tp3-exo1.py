import os
import shutil

src_path = 'flags'
dst_path = 'flagsBis'

if not os.path.exists(dst_path):
    os.makedirs(dst_path)
for file in os.listdir(src_path):
    if file.endswith('.png') and file != 'missing.png':
        initials = file[:2].upper()
        new_file = initials + '.png'
        try:
            shutil.copy(os.path.join(src_path, file), os.path.join(dst_path, new_file))
        except Exception as e:
            print("Erreur lors de la copie du fichier {} : {}".format(file, e))
print("Fichiers copiés avec succès dans le dossier flagsBis")
