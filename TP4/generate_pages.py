import os

from page_maker import PageMaker

csv_file = "deniro.csv"
output_folder = "/tmp"

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

pm = PageMaker(csv_file)
pm.generate_html(output_folder)
