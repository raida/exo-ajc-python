class PageMaker:
    def __init__(self, csv_file):
        self.csv_file = csv_file
        self.actor_name = os.path.splitext(os.path.basename(csv_file))[0].replace("_", " ")
    
    def generate_html(self, output_folder):
        with open(self.csv_file, "r") as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                year, score, title = row
                html = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <title>{actor_name} Movies - {title}</title>
                </head>
                <body>
                  <h1>Film: {title}</h1>
                  <p>Année de sortie: {year}</p>
                  <p>Score obtenue: {score}</p>
                </body>
                </html>
                """.format(actor_name=self.actor_name, title=title, year=year, score=score)
                with open(f"{output_folder}/{title}.html", "w") as out:
                    out.write(html)
