import sys

try:
    number = int(sys.argv[1])
except ValueError:
    print("Veuillez fournir un entier en argument de ligne de commande.")
    sys.exit(1)
if not (0 <= number <= 1000):
    print("Veuillez fournir un entier compris entre 0 et 1000.")
    sys.exit(1)
for i in range(11):
    print(f"{number} x {i} = {number * i}")
