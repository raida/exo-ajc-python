import random

secret = random.randint(1, 10)

while True:
    answer = input("Devine mon chiffre secret: ")
    answer = int(answer)

    if answer == secret:
        break
    elif answer > secret:
        print("Mon chiffre est plus petit")
    else:
        print("Mon chiffre est plus grand")
        
print(f"Bravo ! Le chiffre secret à deviner était bien {secret}")
