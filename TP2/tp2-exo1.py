def calculate_vat(price, vat_rate):
    """Calculer la TVA sur un prix donné en fonction d'un taux de TVA donné"""
    vat = price * vat_rate / 100
    return vat

prices = [14,100,30,10,8]

vat_rate = input("Veuillez saisir un taux de TVA (en %): ")
if not vat_rate.isdigit():
    print("Veuillez saisir un taux de TVA valide (un nombre entier ou à virgule)")
else:
    vat_rate = float(vat_rate)
    for price in prices:
        vat = calculate_vat(price, vat_rate)
        ttc_price = price + vat
        print(f"Prix HT: {price} EUR, TVA ({vat_rate}%): {vat:.2f} EUR, Prix TTC: {ttc_price:.2f} EUR")
