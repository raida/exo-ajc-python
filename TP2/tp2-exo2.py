def get_user_value():
    """Demander à l'utilisateur de saisir un chiffre et vérifier que c'est un nombre entier"""
    value = input("Veuillez saisir un chiffre (0 pour sortir de la boucle): ")
    if not value.isdigit():
        print("Veuillez saisir un chiffre valide")
        return None
    else:
        return int(value)

def update_sum_and_recap(sum, recap, value):
    """Mettre à jour la somme et le récapitulatif en ajoutant un nouveau chiffre"""
    sum += value
    if len(recap) == 0:
        recap = str(value)
    else:
        recap += f", {value}"
    return sum, recap

sum = 0
recap = ""
while True:
    value = get_user_value()
    if value is None:
        continue
    if value == 0:
        break
    sum, recap = update_sum_and_recap(sum, recap, value)
print(f"Somme des valeurs saisies: {sum} ({recap})")
