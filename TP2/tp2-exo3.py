def print_list_inverse(lst):
    """Afficher chaque élément d'une liste dans l'ordre inverse"""
    for i in range(len(lst)-1, -1, -1):
        print(lst[i])

list1 = [10, 20, 30, 40, 50]

print_list_inverse(list1)
