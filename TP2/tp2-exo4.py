
sample_dict = {'a': 100, 'b': 200, 'c': 300}
value = 200
if value in sample_dict.values():
    print(f"{value} present in a dict")
else:
    print(f"{value} not present in a dict")
