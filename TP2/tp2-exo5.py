def get_max_value(lst):
    """Retourner la valeur la plus élevée d'une liste d'entiers"""
    return max(lst)

x = [4, 6, 8, 24, 12, 2]
print(get_max_value(x))
